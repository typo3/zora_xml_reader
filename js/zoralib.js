/**
 * Created by bbaer on 10/28/16.
 */

function zoraLib( query, zoraTemplate ) {

    function formatDate(date) {
        var pubDate = date;

        var pubDay = pubDate.substr(6, 2);
        var pubMonth = getMonth(pubDate.substr(4, 2));
        var pubYear = pubDate.substr(0, 4);

        pubDate = pubDay + ". " + pubMonth + " " + pubYear;

        if (pubDay == "00" && pubMonth == "00") {
            pubDate = pubYear;
        } else if (pubDay == "00") {
            pubDate = pubMonth + " " + pubYear;
        }
        return pubDate;
    }

    function getMonth (month) {
        if (month == "01") {
            month = "January";
        } else if (month == "02") {
            month = "February";
        } else if (month == "03") {
            month = "March";
        } else if (month == "04") {
            month = "April";
        } else if (month == "05") {
            month = "May";
        } else if (month == "06") {
            month = "June";
        } else if (month == "07") {
            month = "July";
        } else if (month == "08") {
            month = "August";
        } else if (month == "09") {
            month = "September";
        } else if (month == "10") {
            month = "October";
        } else if (month == "11") {
            month = "November";
        } else if (month == "12") {
            month = "December";
        }
        return month;
    }

    function formatType(type) {
        if (type == "article") {
            type = "Article";
        } else if (type == "book_section") {
            type = "Book Section";
        } else if (type == "monograph") {
            type = "Monograph";
        } else if (type == "book") {
            type = "Book";
        } else if (type == "dissertation") {
            type = "Dissertation";
        } else if (type == "edited_scientific_work") {
            type = "Edited Scientific Work";
        }
        return type;
    }

    function replaceTemplateVariables(string, eprint, pubDate, pubType) {
        var exp = {
            citation : /(\(\(citation\)\))/g,
            id       : /(\(\(id\)\))/g,
            author   : /(\(\(author\)\))/g,
            title    : /(\(\(title\)\))/g,
            type     : /(\(\(type\)\))/g,
            pubDate  : /(\(\(pubDate\)\))/g,
            coins    : /(\(\(coins\)\))/g
        }

        string = string.replace(exp.citation, eprint.citation);
        string = string.replace(exp.id, eprint.id);
        string = string.replace(exp.author, eprint.author);
        string = string.replace(exp.type, pubType);
        string = string.replace(exp.title, eprint.title);
        string = string.replace(exp.pubDate, pubDate);
        return string;
    }

    (function ($, query, zoraTemplate) {
        $.get(query, function (data) {
            console.log("Got response", data);
            var xmlData = data;
            var $xml = $(xmlData);

            $xml.find("eprint").each(function () {
                var $this = $(this),
                    eprint = {
                        title: $this.find("title").text(),
                        id: $this["0"].attributes["0"].nodeValue,
                        citation: $this.find("citation").text(),
                        author: $this.find("firstauthor").text(),
                        pubDate: $this.find("pubdate").text(),
                        type: $this.find("type").text(),
                        coins: $this.find("coins").text()
                    }

                var pubDate = formatDate(eprint.pubDate);
                var pubType = formatType(eprint.type);

                /* Possible Variables:
                    eprint.title - title of the publications
                    eprint.citation - Authors, Title, Source
                    eprint.type - Set sometimes, can be Article
                    eprint.id - Zora ID for Link
                    eprint.coins - COINS for search engines. Can be added to a span.
                    pubDate - Formatted date value. Shows Year, Month and Year or full date (depending on source)
                    pubType - Formatted type.

                    Template that is displayed:
                */


                var zoraFormatted = replaceTemplateVariables(zoraTemplate, eprint, pubDate, pubType);

                $('#zoraList').append(zoraFormatted);
            });
        });
    })(jQuery, query, zoraTemplate);
}
