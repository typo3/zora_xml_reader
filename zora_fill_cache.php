#!/usr/bin/php
<?php

$query     = "";
$config    = parse_ini_file("config.ini");
$url       = $config["url"];
$path      = $config["export_path"];
$error     = $config["error_logfile"];
$cacheTime = $config["cache_time"];

if (isset($argv[1])) {
    $query = $argv[1];
} else {
    writeError($error, "No arguments were passed to the script");
}

$md5      = md5($query);
$filename = $path . '/' . $md5;
$running  = $filename . ".lock";

// Check if file already exist, else exit with error
if (file_exists($filename)) {
    if (!file_exists($running)) {
        if (time() - filemtime($filename) > $cacheTime) {
            touch($running);
            // file older than a day
            fetchXML($filename, $url, $query);
            unlink($running);
        }
    }
} else {
    writeError($error, "File to update doesn't exist. MD5: " . $md5 . 
                       " Query: " . $query . " Filename: " . $filename);
}

function writeError($path, $string) {
    $errorMsg = date('Y-m-d h:i:s', time()) . ': ' . $string . PHP_EOL;
    file_put_contents($path, $errorMsg, FILE_APPEND);
    exit(1);
}

function fetchXML($filename, $url, $query) {
    $urlWithQuery = $url . $query;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlWithQuery);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $xmlData = curl_exec($ch);
    curl_close($ch);
    file_put_contents($filename, $xmlData);
}

exit(0);
