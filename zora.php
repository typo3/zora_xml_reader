<?php
/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 11/3/16
 * Time: 9:18 AM
 */

$config    = parse_ini_file("config.ini");
$url       = $config["url"];
$folder    = $config["export_path"];
if ($config["cache_time"] > 0) {
    $caching = true;
} else {
    $caching = false;
}

class Data {

    private $query;
    private $md5;

    function  __construct() {
        if (isset($_GET["name"]) || isset($_GET["orcid"])) {
            $this->query = $this->generateQuery();
            $this->md5   = $this->buildMD5();
            //echo "Trying to connect with query: " . $this->query;
        } else {
            echo "Error" . PHP_EOL . "No query is set. Query needs to be defined.";
            exit(1);
        }
    }

    private function generateQuery() {
        $query = "";
        foreach ($_GET as $key => $value) {
            $query .= "&" . $key . "=" . urlencode($value);
        }

        return "?" . substr($query, 1);
    }

    private function buildMD5() {
        $md5 = md5($this->query);
        return $md5;
    }

    /**
     * @return String
     */
    function getQuery() {
        return $this->query;
    }

    /**
     * @return String md5
     */
    function getHash() {
        return $this->md5;
    }
}

class ZoraRequest {
    private $xmlData;
    private $url;
    private $query;
    private $folder;
    private $fileName;
    private $caching;

    function __construct($data, $url, $folder, $caching) {
        $this->url      = $url;
        $this->query    = $data->getQuery();
        $this->folder   = $folder;
        $this->fileName = $folder . "/" . $data->getHash();
        $this->caching  = $caching;

        $this->checkFolder();
    }

    function getXmlData() {
        if ($this->caching) {
            if (file_exists($this->fileName)) {
                $this->xmlData = file_get_contents($this->fileName);
                exec("./zora_fill_cache.php " . escapeshellarg($this->query) . " > /dev/null 2>/dev/null &");
                return true;
            }
        }
        $this->fetchXML();
    }

    function checkFolder() {
        if ($this->caching) {
            if (!file_exists($this->folder)) {
                mkdir($this->folder);
            }
        }
    }

    function fetchXML() {
        $urlWithQuery = $this->url . $this->query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlWithQuery);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $this->xmlData = curl_exec($ch);
        if (curl_errno($ch)) {
    // this would be your first hint that something went wrong
    die('Couldn\'t send request: ' . curl_error($ch));
} else {
    // check the HTTP status code of the request
    $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($resultStatus == 200) {
        // everything went better than expected
    } else {
        // the request did not complete as expected. common errors are 4xx
        // (not found, bad request, etc.) and 5xx (usually concerning
        // errors/exceptions in the remote script execution)

        die('Request failed: HTTP status code: ' . $resultStatus);
    }
}
        curl_close($ch);
        if ($this->caching) {
            file_put_contents($this->fileName, $this->xmlData);
        }
    }

    function outputXML() {
        header("Content-Type: application/xml");
        echo $this->xmlData;
    }
}

$getData = new Data();
$zora    = new ZoraRequest($getData, $url, $folder, $caching);
$zora->getXmlData();
$zora->outputXML();
