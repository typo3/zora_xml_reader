Zora Reader
===========

Read the XML stream, generated by the UZH Zora API (publist).
Optionally the stream will be cached.

The output is customizable by a template.

Requirements
------------

 * Linux Webserver (Apache2 oder nginx)
 * PHP 5.5+
 * php5-cli (only for caching)
 * jQuery
 
Installation
------------

 * Move the files zora.php, zora_fill_cache.php and config.ini to a directory on your webserver.
 * Adjust config.ini to your needs.
 * Check that zora_fill_cache is executable.
 * Upload the js folder.
 * Try to access the zora.php from your Web Browser: zora.php?name=Bisaz
 * Simplest implementation:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
    <script src="js/zoralib.min.js" type="text/javascript"></script>
</head>
<body>

<ul id="zoraList">
</ul>
<script>
    var zoraTemplate = "<li class=\"zora-element\">" +
            "((citation))"
            + "<br> <a href=\"http://www.zora.uzh.ch/" +
            "((id))" +
            "/\" class=\"zora-link\">Zora</a> " +
            "((type))" +
            "</li>";

    zoraLib("https://w20.math.uzh.ch/zora_xml_reader/zora.php?name=Bisaz&op=AND&PY=2008-2012", zoraTemplate);
</script>
</body>
</html>
```

An online example can be viewed at:
https://w20.math.uzh.ch/zora_xml_reader/zora_test.html

Template
--------

The template is created as a variable that you have to pass to the zoraLib.

The variable parts are tokens in the form of ((token)).

Following tokens can currently be placed:

| Token        | Description                                                              |
| ------------ |:------------------------------------------------------------------------ |
| ((id))       | Internal Zora id. Used to build Zora links.                              |
| ((author))   | Name of first author                                                     |
| ((title))    | Title of the work, if supplied. Also appears in Citation.                |
| ((citation)) | Full zora citation string, includes all authors, title, book name, pages |
| ((type))     | Type of the publication (Article, Book Section, Dissertation, etc.)      |
| ((pubDate))  | Formatted date of original publication.                                  |
| ((coins))    | Coins, can be put in span elements for search engines.                   |
